create database pokedex;

create table defense {
    name text;
    defense int;
};

create table vulnerable {
    name text;
};

create table BST {
    name text;
    bs int;
}

insert into defense values("Kingler", 115);
insert into defense values("Onix", 160);
insert into defense values("Cloyster", 180);
insert into defense values("Shellder", 100);
insert into defense values("Mega Slowbro", 180);
insert into defense values("Slowbro", 110);
insert into defense values("Golem", 130);
insert into defense values("Gravler", 115);
insert into defense values("Sandslash", 110);
insert into defense values("Mega Venasaur", 123);

insert into vulnerable("");

insert into BST values("");