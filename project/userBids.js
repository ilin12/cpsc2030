"use strict";

function active() {
    let auc = document.getElementById("active");
    if(auc.style.display == "none") {
        auc.style.display = "block";
    }
    else {
        auc.style.display = "none";
    }
}

function bid() {
    let bids = document.getElementById("show");
    if(bids.style.display == "none") {
        bids.style.display = "block";
    }
    else {
        bids.style.display = "none";
    }
}

function current() {
    let x = document.getElementById("bidWatch");
    if(x.style.display == "none") {
        x.style.display = "block";
    }
    else {
        x.style.display = "none";
    }
}

function history() {
    let y = document.getElementById("bidHistory");
    if(y.style.display == "none") {
        y.style.display = "block";
    }
    else {
        y.style.display = "none";
    }
}