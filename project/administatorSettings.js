"use strict";

function users() {
    let customer = document.getElementById("user");
    if(customer.style.display == "none") {
        customer.style.display = "block";
    }
    else {
        customer.style.display = "none";
    }
}

function auc() {
    let bet = document.getElementById("betting");
    if(bet.style.display == "none") {
        bet.style.display = "block";
    }
    else {
        bet.style.display = "none";
    }
}

function bids() {
    let bidder = document.getElementById("bidAuc");
    if(bidder.style.display == "none") {
        bidder.style.display = "block";
    }
    else {
        bidder.style.display = "none";
    }
}